# MD5SUM

Perform a checksum on a file using the MD5 hash method.

# Contributing

**Would you like to contribute to FreeDOS?** The programs listed here are a great place to start. Most of these do not have a maintainer anymore and need your help to make them better. Here's how to get started:

* __Maintainers__: Let us know if you'd like to take on one of these programs, and we can provide access to the source code repository here. Please use the FreeDOS package structure when you make new releases, including all executables, source code, and metadata.

* __New developers__: We can extend access for you to track issues here.

* __Translators__: Please submit to the [FD-NLS Project](https://github.com/shidel/fd-nls).

_*Make sure to check all source code licenses, especially for any code you might reuse from other projects to improve these programs. Note that not all open source licenses are the same or compatible with one another. (For example, you cannot reuse code covered under the GNU GPL in a program that uses the BSD license.)_

## MD5SUM.LSM

<table>
<tr><td>title</td><td>MD5SUM</td></tr>
<tr><td>version</td><td>3.0a</td></tr>
<tr><td>entered&nbsp;date</td><td>2005-11-21</td></tr>
<tr><td>description</td><td>Perform a checksum using the MD5 hash method</td></tr>
<tr><td>summary</td><td>Perform a checksum on a file using the MD5 hash method.</td></tr>
<tr><td>keywords</td><td>md5, md5sum, checksum, checksummer, cryptographic, sha, crc</td></tr>
<tr><td>author</td><td>Colin Plumb</td></tr>
<tr><td>maintained&nbsp;by</td><td>Blair Campbell &lt;Blairdude AT gmail DOT com&gt;</td></tr>
<tr><td>platforms</td><td>DOS, OpenWatcom 1.3, Pacific C 7.51, Turbo C 2.01, Turbo C++ 1.01</td></tr>
<tr><td>copying&nbsp;policy</td><td>GNU General Public License, Version 2</td></tr>
</table>
