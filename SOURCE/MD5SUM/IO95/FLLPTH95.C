/* $RCSfile: FLLPTH95.C $
   $Locker: ska $	$Name:  $	$State: Exp $

   	char *fullpath(char *buffer, char *path, int buflen)

   	Fully-qualify path.
   	If buffer != NULL, buflen tells its size.
   	If buffer == NULL, a buffer of buflen bytes is malloc'ed.

   	This code requires to define "maximum" values for the filesystem:
   		MAXPATH95		260
   		MAXNAME95		255
	Unfortunately, these values are assumptions for Win95 & DOS only!

	If the path starts with "\\\\" both backslashes remain; that
	gives some support for UNC path spec.

   $Log: FLLPTH95.C $
   Revision 1.2  2000/01/11 09:34:30  ska
   add: support Turbo C v2.01

   Revision 1.1  2000/01/11 09:10:08  ska
   Auto Check-in

*/

#ifdef USE_LFN

#include <assert.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <dos.h>
#include <errno.h>

#include "io95.h"
#include "io95_loc.h"

#ifndef ERANGE
#define ERANGE  14
#endif

/*#ifndef lint
static char const rcsid[] = 
	"$Id: FLLPTH95.C 1.2 2000/01/11 09:34:30 ska Exp ska $";
#endif*/


/*
 * fullpath
 */

char *fullpath95(char *Xbuffer, const char *path, int Xbuflen)
{
#ifndef __TURBOC__
	DOSREGPACK r;
#else
	union REGS r;
	struct SREGS s;
	union REGS br;
	struct SREGS bs;
#define r_flags x.flags
#endif
	assert(path);

	if(Xbuflen <= 0)
		Xbuflen = MAXPATH95;
	if(Xbuffer == NULL && (Xbuffer = (char *)malloc(Xbuflen)) == NULL)
	{
		errno = ENOMEM;
		return NULL;
	}
#ifndef __TURBOC__
	r.r_cl = 0x02;
	r.r_ds = FP_SEG(path);
	r.r_si = FP_OFF(path);
	r.r_es = FP_SEG(Xbuffer);
	r.r_di = FP_OFF(Xbuffer);
	callWin95(0x60, &r);
#else
	r.x.flags = 1;
	r.h.cl = 0x02;
	s.ds = FP_SEG(path);
	r.x.si = FP_OFF(path);
	s.es = FP_SEG(Xbuffer);
	r.x.di = FP_OFF(Xbuffer);
	memcpy(&br, &r, sizeof(r));
	memcpy(&bs, &s, sizeof(bs));
	br.x.ax = 0x7160;
	intdosx(&br, &br, &bs);
	if((r.x.flags = br.x.flags) & 1)
	{
		r.x.ax = 0x60;
		intdosx(&r, &r, &s);
	}
#endif
	errno = (strlen(Xbuffer) > Xbuflen+1) ?
		errno = ERANGE : (r.r_flags & 1) ?
		errno = ENOENT : 0;
	return (errno != 0) ? NULL : Xbuffer;
}

#endif

