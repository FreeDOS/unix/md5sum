/* In the Public Domain by Blair Campbell */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <ctype.h>
#include <io.h>
#include <process.h>
#include <dos.h>

#ifndef F_OK
#define F_OK 00
#endif

short wcc = 0, tcc = 0, pacc = 0, mcc = 0, djgpp = 0, bcc = 0;

void usage(void)
{
	printf("Project Builder 1.0, Public Domain by Blair Campbell\n");
	printf("This utility configures makefiles by selecting a suitable compiler\n");
	printf("and copying the appropriate config.mak file to config.mak\n");
	printf("Options:\n");
	printf("          /C         selects a compiler to use, which could be:\n");
	printf("          /CWCC      OpenWatcom C/C++/Fortran\n");
	printf("          /CTCC      Borland Turbo C 2.01 or greater\n");
	printf("          /CPACC     Pacific C 7.xx\n");
	printf("          /CMCC      Dunfield Micro-C 3.xx\n");
	printf("          /CDJGPP    the DJGPP GNU development system for DOS\n");
	printf("          /CBCC      Borland's C/C++ compiler\n\n");
	exit(0);
}

void checkarg(const char *opt)
{
	short i, len;						/* Define variables */
	
	len=strlen(opt);					/* Find out the length of opt */
	for(i=0;i<=len;i++)					/* Parse options */
	{
		switch(toupper(opt[i]))				/* Make opt upper case */
		{
			case 'H':				/* The recognized help opts */
			case '?':
				usage();			/* Call the help function */
				return;
			case 'C':
				i++;
				if (strnicmp(&opt[i], "wcc", 3) == 0) wcc = 1;
				if (strnicmp(&opt[i], "tcc", 3) == 0) tcc = 1;
				if (strnicmp(&opt[i], "pacc", 4) == 0) pacc = 1;
				if (strnicmp(&opt[i], "mcc", 3) == 0) mcc = 1;
				if (strnicmp(&opt[i], "djgpp", 3) == 0) djgpp = 1;
				if (strnicmp(&opt[i], "bcc", 3) == 0) bcc = 1;
				return;
		}
	}
	return;							/* Return success */
}

int searchcomp(char *name)
{
	char *filename;
#ifdef __TURBOC__
	filename = (char *)searchpath(name);
#else
	_searchenv(name, "PATH", filename);
#endif
	if(filename == NULL) return 0;
	return 1;
}

int fcopy(char *src, char *dst)
{
	FILE *source, *dest;
	char buffer[150];
	int rc;
	source = fopen(src, "r");
	dest = fopen(dst, "w");
	while(fgets(buffer, 150, source) != NULL) fputs(buffer, dest);
	if (ferror(source)||ferror(dest)) rc = 1;
	else rc = 0;
	fclose(source);
	fclose(dest);
	return rc;
}

int getcputype(void)
{
	union REGS regs;
	regs.h.ah = 0xC9;
	regs.h.al = 0x10;
	int86(0x15, &regs, &regs);
	return regs.h.ch;
}

int main(int argc, char **argv)
{
	int i = 1;
	int cpu = getcputype();
	FILE *fp;
	unlink("config.mak");
	unlink("make.bat");
	fp = fopen("make.bat", "w");
	for(;argc > i && (argv[i][0] == '-' || argv[i][0] == '/');i++)
	{
		checkarg(&argv[i][1]);
		if (wcc && cpu >= 3)
		{
			if(searchcomp("WCC.EXE")&&access("config.wat",F_OK)==0)
			{
				fputs("wmake.exe /ms %1 %2 %3 %4 %5 %6 %7 %8 %9", fp);
				return fcopy("config.wat", "config.mak");
			}
		}
		if (tcc)
		{
			if(searchcomp("TCC.EXE")&&access("config.tcc",F_OK)==0)
			{
				if(searchcomp("WMAKE.EXE")) fputs("wmake.exe /ms %1 %2 %3 %4 %5 %6 %7 %8 %9", fp);
				else if(searchcomp("MAKE.EXE")) fputs("make.exe %1 %2 %3 %4 %5 %6 %7 %8 %9", fp);
				return fcopy("config.tcc", "config.mak");
			}
		}
		if (pacc)
		{
			if(searchcomp("PACC.EXE")&&access("config.pac",F_OK)==0)
			{
				if(searchcomp("WMAKE.EXE")) fputs("wmake.exe /ms %1 %2 %3 %4 %5 %6 %7 %8 %9", fp);
				else if(searchcomp("MAKE.EXE")) fputs("make.exe %1 %2 %3 %4 %5 %6 %7 %8 %9", fp);
				return fcopy("config.pac", "config.mak");
			}
		}
		if (mcc)
		{
			if(searchcomp("MCC.EXE")&&access("config.mcc",F_OK)==0)
			{
				if(searchcomp("WMAKE.EXE")) fputs("wmake.exe /ms %1 %2 %3 %4 %5 %6 %7 %8 %9", fp);
				else if(searchcomp("MAKE.EXE")) fputs("make.exe %1 %2 %3 %4 %5 %6 %7 %8 %9", fp);
				return fcopy("config.mcc", "config.mak");
			}
		}
		if (djgpp && cpu >=3)
		{
			if(searchcomp("GCC.EXE")&&access("config.dj",F_OK)==0)
			{
				fputs("make.exe %1 %2 %3 %4 %5 %6 %7 %8 %9", fp);
				return fcopy("config.dj", "config.mak");
			}
		}
		if (bcc)
		{
			if(searchcomp("BCC.EXE")&&access("config.bcc",F_OK)==0)
			{
				fputs("make.exe %1 %2 %3 %4 %5 %6 %7 %8 %9", fp);
				return fcopy("config.bcc", "config.mak");
			}
		}
	}
	unlink("make.bat");
	return 1;
}
