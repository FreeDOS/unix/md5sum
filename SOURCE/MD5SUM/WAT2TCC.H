#ifndef __WAT2TCC_H__
#define __WAT2TCC_H__ 1

#ifdef __TURBOC__

#define __argv _argv
#define __argc _argc

#define _fmalloc farmalloc
#define _ffree farfree
/*#define _dos_allocmem(x,(unsigned short *)y) allocmem(x,y)*/
#define _dos_freemem freemem
#define _harderr harderr
#define _hardresume hardresume
#define _hardretn hardretn

#define __far far
#define _far far

#define find_t ffblk
#define _find_t ffblk
#define name ff_name
#define attrib ff_attrib
#define reserved ff_reserved
#define wr_time ff_ftime
#define wr_date ff_date
#define size ff_fsize
#define _dos_findfirst(x,y,z) findfirst(x,z,y)
#define _dos_findnext findnext
#define _dos_findclose

#define _A_NORMAL FA_RDONLY
#define _A_RDONLY FA_RDONLY
#define _A_SYSTEM FA_SYSTEM
#define _A_SUBDIR FA_DIREC
#define _A_HIDDEN FA_HIDDEN
#define _A_VOLID FA_LABEL
#define _A_ARCH FA_ARCH

#define _mktemp mktemp

#define _dos_getdiskfree getdfree
#define _getdrive() getdisk()+1
#define _dos_setdrive(x,y) setdisk(x-1)
#define _diskfree_t dfree
#define total_clusters df_total
#define avail_clusters df_avail
#define sectors_per_cluster df_sclus
#define bytes_per_sector df_bsec

#define _splitpath fnsplit
#define _makepath fnmerge

#define dosdate_t date
#define year da_year
#define month da_mon
#define day da_day
#define hour ti_hour
#define minute ti_min
#define second ti_sec
#define hsecond ti_hund
#define dostime_t time
#define _dos_getdate getdate
#define _dos_gettime gettime
#define _dos_setdate setdate
#define _dos_settime settime

#define inpw inport
#define outpw outport
#define _enable enable
#define _disable disable

#define _bios_serialcom(x,z,y) bioscom(x,y,z)
#define _bios_equiplist biosequip
#define _bios_keybrd bioskey
#define _bios_printer(x,z,y) biosprint(x,y,z)

#define cflag flags

#define _dos_getvect getvect
#define _dos_setvect setvect
#define _dos_keep keep

#define _DEFAULTMODE LASTMODE
#define _TEXTBW40 BW40
#define _TEXTC40 C40
#define _TEXTBW80 BW80
#define _TEXTC80 C80
#define _TEXTMONO MONO
#define _setvideomode textmode
#define _settextwindow(b,a,d,c) window(a,b,c,d)

#define _settextcursor _setcursortype

#define S_ISDIR( m )    (((m) & S_IFMT) == S_IFDIR)

#define F_OK 00

#define PATH_MAX MAXPATH
#define _MAX_PATH MAXPATH

int snprintf(char * dest_string, int maxlen, const char *format_string, ...);

#endif

#endif
